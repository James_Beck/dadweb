﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace DadWeb.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult AboutDad()
        {
            return View();
        }

        public IActionResult DadLikes()
        {
            return View();
        }

        public IActionResult DadDislikes()
        {
            return View();
        }
    }
}
